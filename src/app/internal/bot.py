from telegram.ext import CommandHandler, Updater

from config.settings import TELEGRAM_BOT_TOKEN

from .transport.bot.handlers import me_command, set_phone_command, start_command


def start_bot():

    updater = Updater(token=TELEGRAM_BOT_TOKEN)
    dispatcher = updater.dispatcher

    add_handler(dispatcher, "start", start_command)
    add_handler(dispatcher, "me", me_command)
    add_handler(dispatcher, "set_phone", set_phone_command)

    updater.start_polling()


def add_handler(dispatcher, name, command):
    handler = CommandHandler(name, command)
    dispatcher.add_handler(handler)
