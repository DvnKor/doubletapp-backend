from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.bot_answer_strings_service import (
    get_hello_message,
    get_phone_error_message,
    get_phone_request_message,
    get_phone_saved_message,
    get_start_request_message,
    get_user_info_message,
)
from app.internal.services.telegram_service import send_message
from app.internal.services.user_service import create_user, get_user_by_id, update_user_phone


def start_command(update: Update, context: CallbackContext):
    send_message(update, context, get_hello_message())

    user = update.effective_user

    user_id = user.id
    name = user.full_name
    login = user.username

    create_user(id=user_id, name=name, login=login)


def me_command(update: Update, context: CallbackContext):
    user_info = get_user_by_id(update.effective_user.id)

    if user_info is None:
        send_message(update, context, get_start_request_message())
        return

    if user_info.phone == "":
        send_message(update, context, get_phone_request_message())
        return

    send_message(
        update, context, get_user_info_message(user_info.name, user_info.login, user_info.phone), is_markdown=True
    )


def set_phone_command(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user_id = telegram_user.id
    saved_user = get_user_by_id(user_id)

    if saved_user is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=get_start_request_message())
        return

    if len(context.args) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=get_phone_error_message(),
        )
        return

    phone = context.args[0]
    update_user_phone(user_id=user_id, phone=phone)

    send_message(update, context, get_phone_saved_message())
