from django.http import JsonResponse
from django.views import View

from app.internal.services.user_service import get_user_by_login


class MeView(View):
    def get(self, request):
        if "login" not in request.GET:
            return JsonResponse(None, safe=False)

        login = request.GET["login"]
        user_info = get_user_by_login(login)
        if user_info is not None:
            return JsonResponse(user_info.to_dictionary())
