from telegram import ParseMode, Update
from telegram.ext import CallbackContext


def send_message(update: Update, context: CallbackContext, message: str, is_markdown: bool = False):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=message, parse_mode=ParseMode.MARKDOWN if is_markdown else None
    )
