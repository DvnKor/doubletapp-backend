from app.internal.models.bot_user import BotUser


def get_user_by_login(login: str):
    return BotUser.objects.filter(login=login).first()


def get_user_by_id(id: int):
    return BotUser.objects.filter(id=id).first()


def create_user(id: str, login: str, name: str):
    BotUser.objects.update_or_create(id=id, name=name, login=login)


def update_user_phone(user_id: int, phone: str):
    BotUser.objects.filter(id=user_id).update(phone=phone)
