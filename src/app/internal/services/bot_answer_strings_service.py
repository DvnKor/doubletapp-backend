def get_hello_message():
    return """Привет!
Вы можете указать номер телефона с помощью команды
/set_phone [ваш номер телефона]"""


def get_start_request_message():
    return "Чтобы воспользоваться этой командой, сначала используйте команду /start"


def get_phone_request_message():
    return """Чтобы воспользоваться этой командой, сначала укажите свой номер телефона с помощью команды
/set_phone [ваш номер телефона]"""


def get_user_info_message(name: str, login: str, phone: str):
    return f"""*Информация о пользователе*

*Имя:* {name}
*Логин:* {login}
*Номер телефона:* {phone}"""


def get_phone_error_message():
    return """Укажите номер телефона в качестве аргумента:
/set_phone [ваш номер телефона]"""


def get_phone_saved_message():
    return "Номер телефона сохранён"
