from django.db import models


class BotUser(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    login = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.login}"

    def to_dictionary(self):
        return {"id": self.id, "name": self.name, "login": self.login, "phone": self.phone}


class Meta:
    verbose_name = "Bot user"
    verbose_name_plural = "Bot users"
