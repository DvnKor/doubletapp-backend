from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin

from .models import BotUser


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
    pass


admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
